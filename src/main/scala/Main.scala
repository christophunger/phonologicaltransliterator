/*
    PhonologicalTransliterator - transliterating from Arabic to Latin script
    Copyright (C) 2021  Christoph Unger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package PhonologyWithTypeclasses

import scala.io.*
import java.nio.file.Files.*
import java.nio.file.Paths.*
import java.nio.file.OpenOption
import java.nio.file.StandardOpenOption
import java.util.*
import collection.JavaConverters.collectionAsScalaIterableConverter
import scala.util.matching.Regex
import scala.util.Try
import PhonologyWithTypeclasses.*
import Phonology.*
import PhonologicalParser.*
import cats.data.*
import PhonologyWithTypeclasses.Phonology
import CharacterMap.*
import Preparation.*
import cats.instances.list

// IO Monad
case class IO[+A](unsafeRun: () => A):
  def >>[B](iob: IO[B]): IO[B] =
    IO.delay {
      this.unsafeRun()
      iob.unsafeRun()
  }
   
  def flatMap[B](f: A => IO[B]): IO[B] = {
    val x = this.map(f)
    IO.flatten(x)
  }
      
  def map[B](f: A => B): IO[B] =
    IO.delay {
      f(unsafeRun())
    }
   
object IO:
  def pure[A](a: A): IO[A] = IO(() => a)
  def delay[A](a: => A): IO[A] = IO(() => a)
  def flatten[A](ioioa: IO[IO[A]]): IO[A] =
    IO.delay(ioioa.unsafeRun().unsafeRun())

// Main program
@main def transliterate(args: String*): Unit = {
  // Reading the obligatory commandline arguments for input and output
  val Seq(input,output) = args
  
  // to make the .jar file work on Windows. From scala.io
  implicit val codec = Codec("UTF-8")

  // Opening the input file
  val workingon = IO.delay(readAllLines(get(input)).asScala.toList).unsafeRun()

  // Core program
  // Prepare the input as a list of strings
  val splittedInput = workingon.flatMap(_.split(" ").toList)

  // map characters from AS to LS
  val charMapped = splittedInput.map(characterTranslit(_))

  // define the post-mapping interpretation pipeline
  def runOnWordsOnly(in: String): String = in match {
    case x if (isSFM(x)) => x
    case x if (!isSFM(x)) => fullTranslitPipe(in)
  }

  def fullTranslitPipe(in: String): String = {
    // disambiguate 'uu' ('uî' is tricky; postpone)
    val charMappedUu = uuToOne(in)
    // separating word from punctuation
    val (punctuation, letters) = charMappedUu partition (x => punct(x))
    // if 'words' is a string of digits, put them out directly
    letters match {
      // assuming that words do not have digits embedded,
      // and strings digits don't have non-digits embedded.
      // Turns out that this is wrong: 12ê (number + case) ordinals
      case x if letters.isEmpty =>
        val (zahlen, kasus) = letters span (x => digits(x)) 
        zahlen ++ kasus ++ punctuation
      case _ =>
        val (word, symbols) = letters partition (x => allPhonemesSet(x))
        if !word.isEmpty() then 
          symbols ++ transformResult(wordp.parse(cleanInput2(word))) ++ punctuation
        else
          symbols ++ punctuation
    }
  }

  /* Pre-filling certain strings with /i/-vowels.
   * Recursive version, better than initial one with manual chaining
   */
  def cleanInput2(clin: String): String = {
    // define some RegExs
    // for some reason, Scala gets confused if I don't prefix List with scala.List
    val pairsForCleaning = scala.List(
      ("bersv", "bersiv"),
      ("serblnd", "serbilind"),
      ("grng", "giring"),
      ("şermz", "şermiz"),
      ("krn", "kirin"))

    def chainCleaning(pairs: scala.List[(String,String)], starttext: String): String = {
      val cleanedHead = pairs.head._1.r.unanchored.replaceAllIn(starttext, pairs.head._2)
      if
        pairs.tail.isEmpty
      then
        cleanedHead
      else
        chainCleaning(pairs.tail, cleanedHead)
    }

    chainCleaning(pairsForCleaning, clin) 
  }

  
  // insert line break before SFM in output
  // SFM: Standard Format Marker https://ubsicap.github.io/usfm/usfm3.0.2/about/index.html
  // Essentially, an SFM is \ followed by one or more non-space characters
  val outSfm = charMapped.map(x => runOnWordsOnly(x))
    .map(x => if (isSFM(x)) "\n" concat x; else x)
    .mkString(" ")

  // // cleanup: remove glottal stop
  // val finalOutput = removeGlottalStop(outSfm)
  val finalOutput = removeRedundantYi(outSfm) // temporary

  //Write the result to output file
  IO.delay(writeString(get(output),finalOutput)).unsafeRun()
}

