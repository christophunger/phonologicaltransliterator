/*
    PhonologicalTransliterator - transliterating from Arabic to Latin script
    Copyright (C) 2021  Christoph Unger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

package PhonologyWithTypeclasses

import scala.util.matching.Regex
import cats.data.*
import PhonologyWithTypeclasses.*
import PhonologyWithTypeclasses.Phonology.Phoneme
import PhonologyWithTypeclasses.Phonology.ConsonantT
import PhonologyWithTypeclasses.Phonology.SoundUT
import PhonologyWithTypeclasses.Phonology.SoundIT
import PhonologyWithTypeclasses.Phonology.VowelT
import PhonologyWithTypeclasses.Phonology.NeutralT
import PhonologyWithTypeclasses.Phonology.NoiseT
import PhonologyWithTypeclasses.Phonology.HighT
import java.{util => ju}

object Phonology {

  // Some character sets
  // sounds grouped by Resonance elements
  val soundUSet = Set('û','u','o','p','b','f','v','m','w')
  val soundRSet = Set('t','d','s','z','n','l','ĺ','r','ŕ','ş','j','ç','c')
  val soundNeutralSet = Set('i','e','u','k','g','x','\u0295')
  val soundASet = Set('a','o','ê','q','\u0127','\u0295','ĺ')
  val soundISet = Set('î','ê','ş','j','ç','c','y')

  // sounds grouped by Manner elements
  val stopSet = Set('ʔ','p','b','t','d','k','g','q','l','ĺ','ç','c')
  val noiseSet = Set('h','f','v','s','z','ş','j','ŕ','ç','c','x','\u0295','\u0127')
  val affricateSet = Set('ç','c')
  val nasalSet = Set('m','n')
  val lateralSet = Set('l','ĺ')
  val noMannerSet = Set('w','y','î','ê','a','û','o','i','e','u','r')

  // sounds grouped by Laryngeal elements
  val lowSet = Set('b','v','m','d','z','n','l','ĺ','j','c','g','\u0295')
  val highSet = Set('p','f','t','s','ş','ç','k','x','q','\u0127')

  // other potentially useful sets
  val longVowelSet = Set('î','ê','a','o','û')
  val shortVowelSet = Set('i','u','e')
  val frontNonLowVSet = Set('î','ê')
  val backNonLowVSet = Set('û','u','o')
  val midNotNeutralVSet = Set('e','a')
  val vowelSet = longVowelSet ++ shortVowelSet
  val allPhonemesSet = soundUSet union soundRSet union soundNeutralSet union soundASet union soundISet union stopSet union noiseSet union affricateSet union nasalSet
  val consonantsSet = allPhonemesSet diff vowelSet
  val continuantSet = Set('m','n','l','ĺ','ŕ','z','j','s','ş')


  sealed trait Phoneme

  sealed trait ConsonantT:
    def sonority: Int
    def withi: Boolean = false

  sealed trait VowelT

/* Using phonological element representations from Harris (1994). 
 *  This is much more economical than the traditional feature-based representation.
 *  But now the sonority scale is more difficult to set up.
 *  Sonority is apparently a function of the Manner elements,
 *  with the exception of /h/. So I assign sonority values to Manner elements,
 * and override them in individual cases where necessary. 
 */

// Resonance
  sealed trait Resonance
  trait SoundAT extends Resonance
  trait SoundIT extends Resonance
  trait SoundUT extends Resonance
  trait NeutralT extends Resonance
  trait CoronalT extends Resonance
  trait NotNeutralT extends Resonance

// Empty or not empty in representation (vowels)
  trait NotfilledT
  trait FilledT

// Manner
  sealed trait Manner
  trait StopT extends Manner:
    def sonority: Int = 10

  trait NoiseT extends Manner:
    def sonority: Int = 20

// AffricateT is an Ad-hoc addition, not in Harris (1994).
// Essentially, it is an abbreviation of the element composition
// of plosives and fricatives together.
// Harris is apparently not very clear about affricates. 
  trait AffricateT extends Manner with StopT with NoiseT:
    override def sonority: Int = 20

  trait NasalT extends Manner:
    // original value 30
    def sonority: Int = 30

/* LateralT is an Ad-hoc addition, not in Harris (1994). 
 * Harris: Laterals have CoronalT and StopT just like plosives,
 * but whereas plosives are headed by StopT,
 * laterals are headed by Coronal. 
 * I don't know how to handle headedness except 
 * by introducing a new trait such as LateralT
 */
  trait LateralT extends Manner with StopT with CoronalT:
    override def sonority: Int = 40

/* This trait serves to get sonority values right.
 * It denotes the absence of a Manner element.  
 */ 
  trait NoMannerT extends Manner:
    def sonority: Int = 40

// Laryngeal
  sealed trait Laryngeal
  trait LowT extends Laryngeal
  trait HighT extends Laryngeal

// Every orthographically represented sound is implemented as a case class.
case class P(p: Char) extends Phoneme
    with ConsonantT
    with SoundUT
    with StopT
    with HighT:
    require(p == 'p')

case class B(b: Char) extends Phoneme
    with ConsonantT
    with SoundUT
    with StopT
    with LowT:
    require(b == 'b')
    override def withi: Boolean = true

case class F(f: Char) extends Phoneme
    with ConsonantT
    with SoundUT
    with NoiseT
    with HighT:
    require(f == 'f')
    override def sonority: Int = 21 // apparently, /fş/ combination in codas is possible. 

case class V(v: Char) extends Phoneme 
    with ConsonantT
    with SoundUT
    with NoiseT
    with LowT:
    require(v == 'v')

case class T(t: Char) extends Phoneme 
    with ConsonantT
    with CoronalT
    with StopT
    with HighT:
    require(t == 't')

case class D(d: Char) extends Phoneme 
    with ConsonantT
    with CoronalT
    with StopT
    with LowT:
    require(d == 'd')

case class S(s: Char) extends Phoneme
    with ConsonantT
    with CoronalT
    with NoiseT
    with HighT:
    require(s == 's')

case class Z(z: Char) extends Phoneme 
    with ConsonantT
    with CoronalT
    with NoiseT
    with LowT:
    require(z == 'z')

case class Sh(sh: Char) extends Phoneme 
    with ConsonantT
    with SoundIT
    with NoiseT
    with HighT:
    require(sh == 'ş')

case class J(j: Char) extends Phoneme 
    with ConsonantT
    with SoundIT
    with NoiseT
    with LowT:
    require(j == 'j')

case class Tsh(c: Char) extends Phoneme 
    with ConsonantT
    with AffricateT
    with SoundIT 
    with HighT:
    require(c == 'ç')

case class Dsh(c: Char) extends Phoneme 
    with ConsonantT
    with AffricateT
    with SoundIT
    with LowT:
    require(c == 'c')

case class K(k: Char) extends Phoneme 
    with ConsonantT
    with NeutralT
    with StopT
    with HighT:
    require(k == 'k')

case class G(g: Char) extends Phoneme 
    with ConsonantT
    with NeutralT
    with StopT
    with LowT:
    require(g == 'g')

case class X(x: Char) extends Phoneme 
    with ConsonantT
    with NeutralT
    with NoiseT
    with HighT:
    require(x == 'x')

case class Xg(x: Char) extends Phoneme 
    with ConsonantT
    with NeutralT
    with NoiseT
    with LowT:
    require(x == '\u0295')

  case class Xw(x: Char) extends Phoneme
      with ConsonantT
      with NeutralT
      with NoiseT
      with SoundUT
      with HighT:
      override def toString: String = "xw"

case class Q(q: Char) extends Phoneme 
    with ConsonantT
    with SoundAT
    with StopT
    with HighT:
    require(q == 'q')

case class Hh(h: Char) extends Phoneme 
    with ConsonantT
    with SoundAT
    with NoiseT
    with HighT:
    require(h == '\u0127')

case class Hhvd(h: Char) extends Phoneme 
    with ConsonantT
    with SoundAT
    with NoiseT
    with LowT:
    require(h == '\u0295')

case class M(m: Char) extends Phoneme 
    with ConsonantT
    with SoundUT
    with NasalT
    with LowT:
    require(m == 'm')

case class N(n: Char) extends Phoneme
    with ConsonantT
    with CoronalT
    with NasalT
    with LowT:
    require(n == 'n')

case class L(l: Char) extends Phoneme 
    with ConsonantT
    with CoronalT
    with LateralT
    with LowT:
    require(l == 'l')


// Sorani velarized /l/, which is used in some Bhd texts  
case class Ll(l: Char) extends Phoneme
      with ConsonantT
      with CoronalT
      with LateralT
      with SoundAT
      with LowT:
      require(l == 'ĺ')


case class R(r: Char) extends Phoneme 
    with ConsonantT
    with CoronalT
    with NoMannerT
    with LowT:
    require(r == 'r')
    override def sonority: Int = 45

case class RR(r: Char) extends Phoneme 
    with ConsonantT
    with CoronalT
    with NoMannerT
    with HighT:
    require(r == 'ŕ')
    override def sonority: Int = 45

case class H(h: Char) extends Phoneme 
    with ConsonantT
    with NoiseT
    with HighT:
    require(h == 'h')
    override def sonority: Int = 40

case class GlottalStop(g: Char) extends Phoneme
    with ConsonantT
    with StopT:
    //    require(g == '\u0294')
    require(g == 'ʔ')

case class W(s: Char) extends Phoneme
      with ConsonantT
      with SoundUT:
      require(s == 'w')
      def sonority: Int = 60

case class Y(s: Char) extends Phoneme
      with ConsonantT
      with SoundIT:
      require(s == 'y')
      def sonority: Int = 60


// Vowels

case class Ii(i: Char) extends Phoneme 
    with VowelT
    with SoundIT
    with NotNeutralT
    with FilledT:
    require(i == 'î')


case class Ui(ui: Char) extends Phoneme
    with VowelT
    with SoundIT
    with SoundUT
    with NotNeutralT
    with FilledT:
    require(ui == 'ü')

case class Ee(e: Char) extends Phoneme 
    with VowelT
    with SoundIT
    with SoundAT
    with NotNeutralT
    with FilledT:
    require(e == 'ê')

case class Uu(u: Char) extends Phoneme
    with VowelT
    with SoundUT
    with NotNeutralT
    with FilledT:
    require(u == 'û')

case class Aa(a: Char) extends Phoneme 
    with VowelT
    with SoundAT
    with NotNeutralT
    with FilledT:
    require(a == 'a')

case class Oo(o: Char) extends Phoneme 
    with VowelT
    with SoundUT
    with SoundAT
    with NotNeutralT
    with FilledT:
    require(o == 'o')

case class E(e: Char) extends Phoneme 
    with VowelT
    with SoundAT
    with NeutralT
    with FilledT:
    require(e == 'e')

case class I(i: Char) extends Phoneme 
    with VowelT
    with NeutralT
    with FilledT:
    require(i == 'i')

  case class U(u: Char) extends Phoneme
      with VowelT
      with NeutralT
      with SoundUT
      with FilledT:
      require(u == 'u')

  // Empty vowel at end of word
  case class EmptyV(ev: Null) extends Phoneme
      with VowelT
      with NeutralT
      with NotfilledT

  // Empty vowel after continuants
  case class SpreadedV(sv: Null) extends Phoneme
      with VowelT
      with NeutralT
      with NotfilledT

  // Empty vowel if neutral vowel is not written in input
  case class NotwrittenI(ni: Null) extends Phoneme
      with VowelT
      with NeutralT
      with NotfilledT

  // Empty consonant for semivowel-disambiguation
  case class SpreadedCons(sc: Null) extends Phoneme
      with ConsonantT:
      def sonority: Int = 60

/* Syllables
 * 
 * CVCV Phonology has it that phonological structure only consists of CVCV... sequences.
 * Some of these slots may not be phonetically realised, depending on their context.
 * In particular, sequences of consonants are underlyingly CVCV sequences with empty Vs.
 *  
 * Since this account does away with the notion of syllable, 
 * we don't need a Syllable class any longer. 
 * 
 */

  // Words are lists of consonant-vowel pairs
  case class Word(w: NonEmptyList[(ConsonantT, VowelT)])

}


