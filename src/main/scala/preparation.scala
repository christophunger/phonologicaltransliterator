/*
    PhonologicalTransliterator - transliterating from Arabic to Latin script
    Copyright (C) 2021  Christoph Unger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

package PhonologyWithTypeclasses

import Phonology.*
import scala.util.matching.Regex

object Preparation {
  // Preparation AND cleanup

  // Interpret sequence 'uu' 
  def uuToOne(in: String): String = {
    val regex = "uu".r
    regex.replaceAllIn(in, "û")
  }


  // Find out if the present char sequence is a SFM
  val sfm = """\\\w+""".r 
  def isSFM(in: String): Boolean = {
    val res = sfm findPrefixOf in
    res match {
      case Some(_) => true
      case None => false
    }
  }

 
  
  def convNumerals(n: Char): Char = {
    // Some input text have a mixed representation of numerals,
    // they must first be converted to Eastern Arabic numerals.
    val digitSpecial = Map(
      '0' -> '٠',
      '1' -> '١',
      '2' -> '٢',
      '3' -> '٣',
      '4' -> '٤',
      '5' -> '٥',
      '6' -> '٦',
      '7' -> '٧',
      '8' -> '٨',
      '9' -> '٩'
    )
    val convertedNumeral = digitSpecial get n
    convertedNumeral match {
      case Some(x) => x
      case None => n
    }
  }

  // Punctuation
  val punct = Set('.',',',':',';','?','!','<','>','(',')')
  val digits = Set('0','1','2','3','4','5','6','7','8','9')

  // Clean up afterwards
  // Remove glottal stop word initially
  def removeGlottalStop(in: String): String = {
    val regex = s"([\\s${punct}()])(\u0294)".r
    regex.replaceAllIn(in, m => m.group(1))
  }

  def removeRedundantYi(in: String): String = {
    val regex = s"(y[îê])(y)".r
    regex.replaceAllIn(in, m => m.group(2))
  }

}
