package PhonologyWithTypeclasses

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import cats.evidence.===
import cats.evidence.Is.refl
import cats.syntax.all.*
import PhonologyWithTypeclasses.*
import Phonology.*
import PhonologicalParser.*
import cats.data.*
import cats.implicits.*
import org.scalatest.OptionValues
import org.scalatest.LoneElement


class TestParser extends AnyFlatSpec with Matchers with OptionValues with LoneElement {


  "1-syllable word /çû/ 'he went' OS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((Tsh(ç),Uu(û)))))"
    assertResult(expectedParse) { wordp.parse("çû").toString }
  }

  it should "spell correctly as 'çû'" in {
    val expectedSpelling = "çû"
    assertResult(expectedSpelling) { transformResult(wordp.parse("çû")).toString }
  }

  "1-syllable word /dil/ 'heart' CS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((D(d),I(i)), (L(l),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("dil").toString }
  }

  it should "spell correctly as 'dil'" in {
    val expectedSpelling = "dil"
    assertResult(expectedSpelling) { transformResult(wordp.parse("dil")).toString }
  }

  "2-syllable word /belku/ 'maybe' CS-OS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((B(b),E(e)), (L(l),SpreadedV(null)), (K(k),U(u)))))"
    assertResult(expectedParse) { wordp.parse("belku").toString }
  }

  it should "spell correctly as 'belku'" in {
    val expectedSpelling = "belku"
    assertResult(expectedSpelling) { transformResult(wordp.parse("belku")).toString }
  }

  "2-syllable word /kirin/ 'to do' OS-CS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((K(k),I(i)), (R(r),I(i)), (N(n),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("kirin").toString }
  }

  it should "spell correctly as 'kirin'" in {
    val expectedSpelling = "kirin"
    assertResult(expectedSpelling) { transformResult(wordp.parse("kirin")).toString }
  }

  "2-syllable word /bingeh/ 'foundation' CS-CS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((B(b),I(i)), (N(n),SpreadedV(null)), (G(g),E(e)), (H(h),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("bingeh").toString }
  }

  it should "spell correctly as 'bingeh'" in {
    val expectedSpelling = "bingeh"
    assertResult(expectedSpelling) { transformResult(wordp.parse("bingeh")).toString }
  }

  "3-syllable word /bînahî/ 'the sight' in oblique case  OS-OS-OS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((B(b),Ii(î)), (N(n),Aa(a)), (H(h),Ii(î)))))"
    assertResult(expectedParse) { wordp.parse("bînahî").toString }
  }

  it should "spell correctly as 'bînahî'" in {
    val expectedSpelling = "bînahî"
    assertResult(expectedSpelling) { transformResult(wordp.parse("bînahî")).toString }
  }


  "3-syllable word /bingehî/ 'foundation' in oblique case CS-OS-OS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((B(b),I(i)), (N(n),SpreadedV(null)), (G(g),E(e)), (H(h),Ii(î)))))"
    assertResult(expectedParse) { wordp.parse("bingehî").toString }
  }

  it should "spell correctly as 'bingehî'" in {
    val expectedSpelling = "bingehî"
    assertResult(expectedSpelling) { transformResult(wordp.parse("bingehî")).toString }
  }



  "3-syllable word /dilnermî/ 'gentleness' CS-CS-OS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((D(d),I(i)), (L(l),SpreadedV(null)), (N(n),E(e)), (R(r),NotwrittenI(null)), (M(m),Ii(î)))))"
    assertResult(expectedParse) { wordp.parse("dilnermî").toString }
  }

  it should "spell correctly as 'dilnermî'" in {
    val expectedSpelling = "dilnermî"
    assertResult(expectedSpelling) { transformResult(wordp.parse("dilnermî")).toString }
  }



  "3-syllable word /nêçîrvan/ 'hunter' OS-CS-CS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((N(n),Ee(ê)), (Tsh(ç),Ii(î)), (R(r),NotwrittenI(null)), (V(v),Aa(a)), (N(n),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("nêçîrvan").toString }
  }

  it should "spell correctly as 'nêçîrvan'" in {
    val expectedSpelling = "nêçîrvan"
    assertResult(expectedSpelling) { transformResult(wordp.parse("nêçîrvan")).toString }
  }



  "3-syllable word /dergehvan/ 'doorkeeper' CS-CS-CS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((D(d),E(e)), (R(r),NotwrittenI(null)), (G(g),E(e)), (H(h),NotwrittenI(null)), (V(v),Aa(a)), (N(n),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("dergehvan").toString }
  }

  it should "spell correctly as 'dergehvan'" in {
    val expectedSpelling = "dergehvan"
    assertResult(expectedSpelling) { transformResult(wordp.parse("dergehvan")).toString }
  }



  "4-syllable word /dadkirina/ 'the conviction of x' CS-OS-OS-OS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((D(d),Aa(a)), (D(d),NotwrittenI(null)), (K(k),I(i)), (R(r),I(i)), (N(n),Aa(a)))))"
    assertResult(expectedParse) { wordp.parse("dadkirina").toString }
  }

    it should "spell correctly as 'dadkirina'" in {
    val expectedSpelling = "dadkirina"
    assertResult(expectedSpelling) { transformResult(wordp.parse("dadkirina")).toString }
  }



// "Sonority values get transmitted"  should "work as expected" in {
//   val expectedParse = ???
//   assertResult(expectedParse) { wordp.parse("").toString }
// }

  "1-syllable word /germ/ 'warm, hot' DCS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((G(g),E(e)), (R(r),NotwrittenI(null)), (M(m),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("germ").toString }
  }

  it should "spell predictably but unconventionally as 'gerim'" in {
    // 'germ' does not pattern with 'kirim' ("I do/make") and 'kirim' ("worm")
    // there doesn't seem to be a general rule how to single out the spelling 'germ'
    val expectedSpelling = "gerim"
    assertResult(expectedSpelling) { transformResult(wordp.parse("germ")).toString }
  }



  "2-syllable word /guvend/ 'dance' OC-DCS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((G(g),U(u)), (V(v),E(e)), (N(n),SpreadedV(null)), (D(d),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("guvend").toString }
  }

  it should "spell correctly as 'guvend'" in {
    val expectedSpelling = "guvend"
    assertResult(expectedSpelling) { transformResult(wordp.parse("guvend")).toString }
  }



  "2-syllable word /hijmart/ 'he counted' CS-DCS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((H(h),I(i)), (J(j),SpreadedV(null)), (M(m),Aa(a)), (R(r),NotwrittenI(null)), (T(t),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("hijmart").toString }
  }

  it should "spell correctly as 'hijmart'" in {
    val expectedSpelling = "hijmart"
    assertResult(expectedSpelling) { transformResult(wordp.parse("hijmart")).toString }
  }



  "2-syllable word /rastgo/ 'honest' DCS-OS"  should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((R(r),Aa(a)), (S(s),SpreadedV(null)), (T(t),NotwrittenI(null)), (G(g),Oo(o)))))"
    assertResult(expectedParse) { wordp.parse("rastgo").toString }
  }

  it should "spell correctly as 'rastgo'" in {
    val expectedSpelling = "rastgo"
    assertResult(expectedSpelling) { transformResult(wordp.parse("rastgo")).toString }
  }



  "2-syllable word /pirsîyar/ 'question' DCS-CS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((P(p),I(i)), (R(r),NotwrittenI(null)), (S(s),Ii(î)), (Y(y),Aa(a)), (R(r),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("pirsîyar").toString }
  }

  it should "spell correctly as 'pirsîyar'" in {
    val expectedSpelling = "pirsîyar"
    assertResult(expectedSpelling) { transformResult(wordp.parse("pirsîyar")).toString }
  }



  "2-syllable word /piştrast/ 'upright' DCS-DCS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((P(p),I(i)), (Sh(ş),SpreadedV(null)), (T(t),NotwrittenI(null)), (R(r),Aa(a)), (S(s),SpreadedV(null)), (T(t),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("piştrast").toString }
  }

  it should "spell by way of overgeneration as 'piştirast'" in {
    val expectedSpelling = "piştirast"
    assertResult(expectedSpelling) { transformResult(wordp.parse("piştrast")).toString }
  }



  "3-syllable word /peristgeh/ 'temple' OS-DCS-CS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((P(p),E(e)), (R(r),I(i)), (S(s),SpreadedV(null)), (T(t),NotwrittenI(null)), (G(g),E(e)), (H(h),EmptyV(null)))))"
      assertResult(expectedParse) { wordp.parse("peristgeh").toString }
  }

  it should "spell correctly as 'peristgeh'" in {
    val expectedSpelling = "peristgeh"
    assertResult(expectedSpelling) { transformResult(wordp.parse("peristgeh")).toString }
  }



  "3-syllable word /behnfireh/ 'relaxed' DCS-OS-CS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((B(b),E(e)), (H(h),NotwrittenI(null)), (N(n),SpreadedV(null)), (F(f),I(i)), (R(r),E(e)), (H(h),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("behnfireh").toString }
  }

  it should "spell correctly as 'behnfireh'" in {
    val expectedSpelling = "behnfireh"
    assertResult(expectedSpelling) { transformResult(wordp.parse("behnfireh")).toString }
  }



  "4-syllable word /desthilatî/ 'power' DCS-OS-CS-OS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((D(d),E(e)), (S(s),SpreadedV(null)), (T(t),NotwrittenI(null)), (H(h),I(i)), (L(l),Aa(a)), (T(t),Ii(î)))))"
    assertResult(expectedParse) { wordp.parse("desthilatî").toString }
  }

  it should "spell by way of overgeneration as 'destihilatî'" in {
    val expectedSpelling = "destihilatî"
    assertResult(expectedSpelling) { transformResult(wordp.parse("desthilatî")).toString }
  }



  "1-syllable word with additional 's' before onset /stand/ 'he took he received' ExtraS-DCS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((S(s),SpreadedV(null)), (T(t),Aa(a)), (N(n),SpreadedV(null)), (D(d),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("stand").toString }
  }

  it should "spell correctly as 'stand'" in {
    val expectedSpelling = "stand"
    assertResult(expectedSpelling) { transformResult(wordp.parse("stand")).toString }
  }



  "1-syllable word with initial 's' as onset /sund/ 'oath' DCS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((S(s),U(u)), (N(n),SpreadedV(null)), (D(d),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("sund").toString }
  }

  it should "spell correctly as 'sund'" in {
    val expectedSpelling = "sund"
    assertResult(expectedSpelling) { transformResult(wordp.parse("sund")).toString }
  }



  "1-syllable word with additional 's' before onset /stêr/ 'star' ExtraS-CS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((S(s),SpreadedV(null)), (T(t),Ee(ê)), (R(r),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("stêr").toString }
  }

    it should "spell correctly as 'stêr'" in {
    val expectedSpelling = "stêr"
    assertResult(expectedSpelling) { transformResult(wordp.parse("stêr")).toString }
  }



  "1-syllable word with initial 's' as onset /sar/ 'cold' CS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((S(s),Aa(a)), (R(r),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("sar").toString }
  }

    it should "spell correctly as 'sar'" in {
    val expectedSpelling = "sar"
    assertResult(expectedSpelling) { transformResult(wordp.parse("sar")).toString }
  }



  "1-syllable word with additional 's' before onset /spî/ 'white' ExtraS-OS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((S(s),SpreadedV(null)), (P(p),Ii(î)))))"
    assertResult(expectedParse) { wordp.parse("spî").toString }
  }

    it should "spell correctly as 'spî'" in {
    val expectedSpelling = "spî"
    assertResult(expectedSpelling) { transformResult(wordp.parse("spî")).toString }
  }



  "1-syllable word with initial 's' as onset /se/ 'dog' OS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((S(s),E(e)))))"
    assertResult(expectedParse) { wordp.parse("se").toString }
  }

    it should "spell correctly as 'se'" in {
    val expectedSpelling = "se"
    assertResult(expectedSpelling) { transformResult(wordp.parse("se")).toString }
  }



  "1-syllable word with additional 'ş' before onset /şkand/ 'it is broken' ExtraS-DCS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((Sh(ş),SpreadedV(null)), (K(k),Aa(a)), (N(n),SpreadedV(null)), (D(d),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("şkand").toString }
  }

    it should "spell correctly as 'şkand'" in {
    val expectedSpelling = "şkand"
    assertResult(expectedSpelling) { transformResult(wordp.parse("şkand")).toString }
  }



  "1-syllable word with additional 'st' before onset /stran/ 'song' ExtraSt-DCS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((S(s),SpreadedV(null)), (T(t),NotwrittenI(null)), (R(r),Aa(a)), (N(n),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("stran").toString }
  }

    it should "spell correctly but unconventionally as 'stiran'" in {
    val expectedSpelling = "stiran"
    assertResult(expectedSpelling) { transformResult(wordp.parse("stran")).toString }
  }



  "1-syllable word with rounded velar fricative in onset /xwar/ 'he ate' in simple past CS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((xw,Aa(a)), (R(r),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("xwar").toString }
  }

    it should "spell correctly as 'xwar'" in {
    val expectedSpelling = "xwar"
    assertResult(expectedSpelling) { transformResult(wordp.parse("xwar")).toString }
  }



  "2-syllable word with rounded velar fricative in onset of 2nd syllable /dixwar/ 'he ate' in imperfective past OS-CS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((D(d),I(i)), (xw,Aa(a)), (R(r),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("dixwar").toString }
  }

    it should "spell correctly as 'dixwar'" in {
    val expectedSpelling = "dixwar"
    assertResult(expectedSpelling) { transformResult(wordp.parse("dixwar")).toString }
  }



  "1-syllable word with unrounded velar fricative on onset /xêr/ 'grace' CS" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((X(x),Ee(ê)), (R(r),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("xêr").toString }
  }

    it should "spell correctly as 'xêr'" in {
    val expectedSpelling = "xêr"
    assertResult(expectedSpelling) { transformResult(wordp.parse("xêr")).toString }
  }



  "A rounded velar fricative must be followed by a vowel: /xwîn/ 'blood' works." should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((xw,Ii(î)), (N(n),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("xwîn").toString }
  }

    it should "spell correctly as 'xwîn'" in {
    val expectedSpelling = "xwîn"
    assertResult(expectedSpelling) { transformResult(wordp.parse("xwîn")).toString }
  }



  // "A rounded velar fricative must be followed by a vowel: */daxw/ non-existing word does not work."  should "parse as expected" in {
  //   val expectedParse = Left(Error)
  //   assertResult(expectedParse) { wordp.parse("daxw").toString }
  // }

  // "A round velar fricative must be followed by a vowel: */doxwtor/ non-existing word does not work." should "parse as expected" in {
  //   val exLeft
  // assertResult(expectedParse) { wordp.parseAll("doxwtor").toString }
  // }

  "An unrounded velar fricative need not be followed by a vowel: /doxtor/ 'physician' works." should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((D(d),Oo(o)), (X(x),NotwrittenI(null)), (T(t),Oo(o)), (R(r),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("doxtor").toString }
  }

    it should "spell correctly as 'doxtor'" in {
    val expectedSpelling = "doxtor"
    assertResult(expectedSpelling) { transformResult(wordp.parse("doxtor")).toString }
  }



  "A rounded velar fricative must be followed by a vowel: /daxwaz/ 'request' works" should "parse as expected" in {
    val expectedParse = "Right((,NonEmptyList((D(d),Aa(a)), (xw,Aa(a)), (Z(z),EmptyV(null)))))"
    assertResult(expectedParse) { wordp.parse("daxwaz").toString }
  }

    it should "spell correctly as 'daxwaz'" in {
    val expectedSpelling = "daxwaz"
    assertResult(expectedSpelling) { transformResult(wordp.parse("daxwaz")).toString }
  }

  "The character u0294" should "be parsed" in {
    val expectedParse = "Right((,GlottalStop(\u0294)))"
    val parsedGlottal = VlGlottalStopp.parse("\u0294").toString
    assertResult(expectedParse) { parsedGlottal }
  }

  it should "be part of the consonantsSet" in {
    assertResult(true) { consonantsSet('\u0294') }
  }

  it should "have trait ConsonantT" in {
    val parsedGlottal = VlGlottalStopp.parse("\u0294")
    val hasConsonantTrait = parsedGlottal match {
      case Right((_,GlottalStop('\u0294'))) => true
      case _ => false }
    assertResult(true) { hasConsonantTrait }
    }

  "The character 'b'" should "be parsed" in {
    val expectedParse = "Right((,B(b)))"
    val parsedBilab = VdbilPlosp.parse("b").toString
    assertResult(expectedParse) { parsedBilab }
  }

  it should "be part of the consonantsSet" in {
    assertResult(true) { consonantsSet('b') }
  }

  it should "have trait ConsonantT" in {
    val parsedBilab = VdbilPlosp.parse("b")
    val hasConsonantTrait = parsedBilab match {
      case Right((_,B(_))) => true
      case _ => false }
    assertResult(true) { hasConsonantTrait }
    }

  "The character '\u0295' vd phar fricative" should "be parsed" in {
    val expectedParse = "Right((,Hhvd(\u0295)))"
    val parsed = postvelarp.parse("\u0295").toString
    assertResult(expectedParse) { parsed }
  }

  it should "be part of the consonantsSet" in {
    assertResult(true) { consonantsSet('\u0295') }
  }

  it should "have trait ConsonantT" in {
    val parsedHhvd = postvelarp.parse("\u0295")
    val hasConsonantTrait = parsedHhvd match {
      case Right((_,Hhvd('\u0295'))) => true
      case _ => false }
    assertResult(true) { hasConsonantTrait }
    }

  "The pronoun _wan_" should "parse with final _n_" in {
    val expected = "Right((,NonEmptyList((W(w),Aa(a)), (N(n),EmptyV(null)))))"
    val parsed = wordp.parse("uan").toString
    assertResult(expected) { parsed }
  }

  "The parsed representation of _wan_" should "spellout as _wan_" in {
    val expected = "wan"
    val formtospell = wordp.parse("uan")
    val spelledout = transformResult(formtospell)
    assertResult(expected) { spelledout }
  }

  "The pronoun wî" should "parse correctly" in {
    val expected = "Right((,NonEmptyList((W(w),Ii(î)))))"
    val parsed = wordp.parse("uî").toString
    assertResult(expected) { parsed }
  }

  "The word /ser/" should "parse correctly" in {
    val expected = "Right((,NonEmptyList((S(s),E(e)), (R(r),EmptyV(null)))))"
    val parsed = wordp.parse("ser").toString
    assertResult(expected) { parsed }
  }

  "The word /serbilind/" should "parse correctly as /serbilind/" in {
    val expected = "Right((,NonEmptyList((S(s),E(e)), (R(r),NotwrittenI(null)), (B(b),I(i)), (L(l),I(i)), (N(n),SpreadedV(null)), (D(d),EmptyV(null)))))"
    val parsed = wordp.parse("serbilind").toString
    assertResult(expected) { parsed }
  }

}
