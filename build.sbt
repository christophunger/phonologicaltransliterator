val scala3Version = "3.3.1"

lazy val root = project
  .in(file("."))
  .settings(
    name := "PhonologyWithTypeclasses",
    version := "0.1.0-SNAPSHOT",

    scalaVersion := scala3Version,

    //    libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % "test"
    libraryDependencies ++= Seq(
      "org.typelevel" %% "cats-parse" % "0.3.4",
      "org.scalameta" %% "munit" % "0.7.29" % Test,
        //  "org.scalameta" %% "munit" % "0.7.29"
      "org.scalactic" %% "scalactic" % "3.2.10",
      "org.scalatest" %% "scalatest" % "3.2.10" % "test",
      "org.scalatest" %% "scalatest-flatspec" % "3.2.10" % "test"


    )

  )

testFrameworks += new TestFramework("munit.Framework")



 
