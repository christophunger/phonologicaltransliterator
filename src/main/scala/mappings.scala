/*
    PhonologicalTransliterator - transliterating from Arabic to Latin script
    Copyright (C) 2021  Christoph Unger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package PhonologyWithTypeclasses

object CharacterMap {

  // Digits, word characters and punctuation.
  // Inspired by ku.py by Dolan Hêrîş (https://github.com/dolanskurd/Kurdish),
  // but thoroughly modified to yield a phonologically rich 
  // intermediary representation. 
  // The goal is a one-to-one mapping from Arabic-script
  // code points to Latin-script code points
  // as a basis for the subsequent processes of
  // semivowel disambiguation and the insertion of the neutral vowel,
  // and final clean-up at the very end. 
  // The representation includes glottal stop (which is not represented in
  // Latin script) and voiced and voiceless pharngeal fricatives
  // (which are only partially distinguished in Latin script).
  // Also, flapped and trilled /r/ are represented as one
  // code point each instead of using double letters 'rr' for the trill.
  // Semivowels are not yet distinguished from vowels. 
  val characters = Map(
    "٠" -> '0',  //  U+0660 to U+0030
    "١" -> '1',  //  U+0661 to U+0031
    "٢" -> '2',  //  U+0662 to U+0032
    "٣" -> '3',  //  U+0663 to U+0033
    "٤" -> '4',  //  U+0664 to U+0034
    "٥" -> '5',  //  U+0665 to U+0035
    "٦" -> '6',  //  U+0666 to U+0036
    "٧" -> '7',  //  U+0667 to U+0037
    "٨" -> '8',  //  U+0668 to U+0038
    "٩" -> '9',  //  U+0669 to U+0039
    "ئ" -> '\u0294',  //  U+0626 to U+0294 glottal stop
    "ا" -> 'a',  //  U+0627 to U+0061
    "ب" -> 'b',  //  U+0628 to U+0062
    "پ" -> 'p',  //  U+067E to U+0070
    "ت" -> 't',  //  U+062A to U+0074
    "ج" -> 'c',  //  U+062C to U+0063
    "چ" -> 'ç',  //
    "ح" -> '\u0127',  //  U+062D to U+0127
    "خ" -> 'x',  //  U+062E to U+0078
    "د" -> 'd',  //  U+062F to U+0064
    "ر" -> 'r',  //  U+0631 to U+0072
    "ڕ" -> 'ŕ',  //
    "ز" -> 'z',  //  U+0632 to U+007A
    "ژ" -> 'j',  //  U+0698 to U+006A
    "س" -> 's',  //  U+0633 to U+0073
    "ش" -> 'ş',  //
    "ع" -> '\u0295',  //  U+0639 to U+0295 pharyngeal fricative
    "غ" -> '\u1E8D',  //  U+063A to U+1E8D
    "ف" -> 'f',  //  U+0641 to U+0066
    "ڤ" -> 'v',  //  U+06A4 to U+0076
    "ق" -> 'q',  //  U+0642 to U+0071
    "ک" -> 'k',  //  U+06A9 to U+006B
    "ك" -> 'k',  // 
    "گ" -> 'g',  //  U+06AF to U+0067
    "ل" -> 'l',  //  U+0644 to U+006C
    "ڵ" -> 'ĺ',  //
    "م" -> 'm',  //  U+0645 to U+006D
    "ن" -> 'n',  //  U+0646 to U+006E
    "و" -> 'u',  //  U+0648 to U+0077
    "ۆ" -> 'o',  //  U+06C6 to U+006F
    "ه" -> 'h',  //  U+0647 to U+0068 (NOT U+06BE 'ھ')
    "ە" -> 'e',  //  U+06D5 to U+0065
    "ة" -> 'e',   // Arabic character in quotation from Arabic
    "ی" -> 'î',  //  U+06CC to U+0079
    "ێ" -> 'ê',  //  U+06CE to U+0059
    "ي" -> 'î',  // Arabic character in quotation from Arabic
    "ط" -> 't',  // Arabic character in quotation from Arabic
    "،" -> ',',  //  U+060C to U+002C
    "؟" -> '?',  //  to U+061F to U+003F
    "؛" -> ';',  //  to U+061B to U+003B
    "‹" -> '<',  // research code points; first attempt
    "›" -> '>',
    "(" -> '(',
    ")" -> ')'
  )

  def characterTranslit(in: String): String = {
    in.map(c => convChar(c))
  }

  def convChar(c: Char): Char = {
    val converted = characters get c.toString
    converted match {
      case Some(x) => x  // mappings defined in the Map of characters are applied
      case None => c  // anything not in the Map isn't touched
    }
  }

}
