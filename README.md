# PhonologicalTransliterator

This is **work in progress**. 

## Purpose

This program converts Behdînî-Kurdish texts written in Arabic script into Latin script. 
It attempts to solve the following problems: 

	- The disambiguation between the vowels 'î' and 'û' and their 
	  corresponding semivowels 'y' and 'w'. 
	- The insertion of the neutral vowel 'i' after and between certain consonant 
      sequences, according to syllable structure rules. 
	  
While solving the practical task of transliteration from Arabic script to Latin script 
is the most tangible goal of this program, 
this software was developped for other reasons as well: 

    - To test theoretically grounded phonological analyses of Behdînî-Kurdish 
	  metrical phonological phenomena.
	- To implement a syllable analyser with the cats.parse library. 
    - For the main author to practice programming principles in Scala (https://scala-lang.org/). 
	- To progress beyond the scope of bhd-transliterator 
	(https://bitbucket.org/christophunger/bhd-transliterator/src/master/)
	
For these reasons the internals of this program may change significantly 
in the future 
as new linguistic insights into Kurdish phonology are gained, 
or new programming techniques become available. 

## Prerequisites and installation instructions

This program is written in Scala 3 (https://scala-lang.org/) 
and compiles to Java Virtual Machine (JVM) bytecode. 
To compile and run this program, you need to have Java 11 LTS installed on your machine. 
More recent Java versions may also work but are untested. 

You can install this software by cloning this repository with Git (recommended), 
or downloading the repository as a .zip file 
and building from source.
For this process, 
you need the Simple Build Tool for Scala (sbt) 
installed on your system, see https://www.scala-sbt.org/

After cloning the repository of bhd-transliterator with Git 
or downloading and uncompressing its .zip file, 
cd into your local repository directory
and run sbt from the commandline. 
This starts the sbt console. 
The first time sbt is run in this directory, 
it will download all necessary Scala files and dependcies 
for compiling and running the program. 

Once the sbt console is done with the initial setup, 
you can then run the program 
from the sbt console by typing the following command: 

```
run infile outfile
```

## Phonological analysis

This work is based on a phonological analysis of syllable structure.
This analysis is explained in the document

```
phonology_writeup.tex
```
To typeset this document as PDF,
process it with \XeLaTeX (a standard TeXLive installation should work),
making sure you have the Gentium Plus font installed
(https://software.sil.org/gentium/download/). 

## Related software

A related software project is the Kurdish Language Library (KLL) by Dolan Hêrîş at Github: 
https://github.com/dolanskurd/kurdish

The Kurdish Language Library is a library written in Python 
providing convenience functions for dealing with Kurdish NLP, 
including script conversions in both directions between 
Arabic script and Latin script, for Soranî as well as Behdînî. 
This library uses a string-based approach to the problem of 
/i/ - insertion and semivowel disambiguation. 
KLL also provides a GUI and a web interface. 

I have written another program, 
Bhd-transliterator (https://bitbucket.org/christophunger/bhd-transliterator/src/master/), 
which, has a narrower focus: 
it provides Arabic script to Latin script conversion only, 
and is only designed for the Behdînî dialect of Kurdish. 
It does not provide other convenience functions for NLP. 
Its approach to /i/ - insertion and semivowel disambiguation 
is based on heuristic processes modelled after a linguistic 
analysis of the metrical phonology of Kurdish, 
and does not use a string-based approach. 
Neither does it perform syllable parsing, 
in contrast to PhonologicalTransliterator.

## License

    PhonologicalTransliterator - transliterating from Arabic to Latin script
    Copyright (C) 2021  Christoph Unger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
