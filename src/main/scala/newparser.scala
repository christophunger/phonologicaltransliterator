/*
    PhonologicalTransliterator - transliterating from Arabic to Latin script
    Copyright (C) 2021  Christoph Unger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

package PhonologyWithTypeclasses

import PhonologyWithTypeclasses.Phonology.*
import cats.parse.*
import cats.data.*
import cats.instances.try_


object PhonologicalParser {

  val VlbilPlosp = Parser
    .charWhere(c => c == 'p')
    .map(c => P(c))

  val VdbilPlosp = Parser
    .charWhere(c => c == 'b')
    .map(c => B(c))

  val VlLdFricp = Parser
    .charWhere(c => c == 'f')
    .map(c => F(c))

  val VdLdFricp = Parser
    .charWhere(c => c == 'v')
    .map(c => V(c))

  val VlCorPlosp = Parser
    .charWhere(c => c == 't')
    .map(c => T(c))

  val VdCorPlosp = Parser
    .charWhere(c => c == 'd')
    .map(c => D(c))

  val VlCorSibp = Parser
    .charWhere(c => c == 's')
    .map(c => S(c))

  val VdCorSibp = Parser
    .charWhere(c => c == 'z')
    .map(c => Z(c))

  val VlCorPalFricp = Parser
    .charWhere(c => c == 'ş')
    .map(c => Sh(c))

  val VdCorPalFricp = Parser
    .charWhere(c => c == 'j')
    .map(c => J(c))

  val VlCorPalAffricp = Parser
    .charWhere(c => c == 'ç')
    .map(c => Tsh(c))

  val VdCorPalAffricp = Parser
    .charWhere(c => c == 'c')
    .map(c => Dsh(c))

  val VlVelPlosp = Parser
    .charWhere(c => c == 'k')
    .map(c => K(c))

  val VdVelPlosp = Parser
    .charWhere(c => c == 'g')
    .map(c => G(c))

  val VlVelFricp = Parser
    .charWhere(c => c == 'x')
    .map(c => X(c))

  val VdVelFricp = Parser
    .charWhere(c => c == '\u0295')
    .map(c => Xg(c))

  val VdUvulPlosp = Parser
    .charWhere(c => c == 'q')
    .map(c => Q(c))

  val VlPharFricp = Parser
    .charWhere(c => c == '\u0127')
    .map(c => Hh(c))

//val VdPharFricp = ???

  val VdBilNasalp = Parser
    .charWhere(c => c == 'm')
    .map(c => M(c))

  val VdCorNasalp = Parser
    .charWhere(c => c == 'n')
    .map(c => N(c))

  val VdCorLatp = Parser
    .charWhere(c => c == 'l')
    .map(c => L(c))

  val VdCorLatVelarp = Parser
    .charWhere(c => c == 'ĺ')
    .map(c => Ll(c))

  val VdCorFlapp = Parser
    .charWhere(c => c == 'r')
    .map(c => R(c))

  val VdCorTrillp = Parser
    .charWhere(c => c == 'ŕ')
    .map(c => RR(c))

  val VlAspp = Parser
    .charWhere(c => c == 'h')
    .map(c => H(c))

  val VlGlottalStopp = Parser
    .charWhere(c => c == '\u0294')
    .map(c => GlottalStop(c))

  val VdBlSemiVp = Parser
    .charWhere(c => c == 'w')
    .map(c => W(c))

  val VdPalSemiVp = Parser
    .charWhere(c => c == 'y')
    .map(c => Y(c))

  // special case: rounded velar fricative "xw" in "xwarin"
  val VlVelRoundFricp =
    ((Parser.charWhere(x => x == 'x').soft <* VdBlSemiVp)
      ~ Parser.charWhere(c => vowelSet(c)).peek)
      .mapFilter {
        case (a,_) => Some(Xw(a))
        case null => None }.backtrack


  // Parsing consonant types
  // Plosives, 'l', and affricates
  val plosivep = Parser
    .charWhere(c => stopSet(c))
    .mapFilter {
      case x if x == '\u0294' => Some(GlottalStop(x))
      case x if x == 'p' => Some(P(x))
      case x if x == 'b' => Some(B(x))
      case x if x == 't' => Some(T(x))
      case x if x == 'd' => Some(D(x))
      case x if x == 'k' => Some(K(x))
      case x if x == 'g' => Some(G(x))
      case x if x == 'q' => Some(Q(x))
      case x if x == 'l' => Some(L(x))
      case x if x == 'ĺ' => Some(Ll(x))
      case x if x == 'ç' => Some(Tsh(x))
      case x if x == 'c' => Some(Dsh(x))
      case _ => None
    }.backtrack

  // Fricatives and affricates
  val fricativep = Parser
    .charWhere(c => noiseSet(c))
    .mapFilter {
      case x if x == 'h' => Some(H(x))
      case x if x == 'f' => Some(F(x))
      case x if x == 'v' => Some(V(x))
      case x if x == 's' => Some(S(x))
      case x if x == 'z' => Some(Z(x))
      case x if x == 'ş' => Some(Sh(x))
      case x if x == 'j' => Some(J(x))
      case x if x == 'ŕ' => Some(RR(x))
      case x if x == 'ç' => Some(Tsh(x))
      case x if x == 'c' => Some(Dsh(x))
      case x if x == 'x' => Some(X(x))
      case x if x == '\u0295' => Some(Xg(x))
      case x if x == '\u0127' => Some(Hh(x))
      case _ => None
    }.backtrack

  // Nasals
  val nasalp = Parser
    .charWhere(c => nasalSet(c))
    .mapFilter {
      case x if x == 'm' => Some(M(x))
      case x if x == 'n' => Some(N(x))
      case _ => None
    }.backtrack

  // Liquids
  val liquidp = Parser.oneOf(VdCorLatp :: VdCorLatVelarp :: VdCorFlapp :: VdCorTrillp :: Nil)

  // Semivowels?
  val semivowelp = Parser
    .charWhere(c => Set('w','y')(c))
    .mapFilter {
      case x if x == 'w' => Some(W(x))
      case x if x == 'y' => Some(Y(x))
      case _ => None
    }.backtrack

  // Labial consonants
  val labialp = Parser
    .charWhere(c => soundUSet(c))
    .mapFilter {
      case x if x == 'p' => Some(P(x))
      case x if x == 'b' => Some(B(x))
      case x if x == 'f' => Some(F(x))
      case x if x == 'v' => Some(V(x))
      case x if x == 'm' => Some(M(x))
      case x if x == 'w' => Some(W(x))
      case _ => None
    }.backtrack

  // Coronal consonants
  val coronalp = Parser
    .charWhere(c => soundRSet(c))
    .mapFilter {
      case x if x == 't' => Some(T(x))
      case x if x == 'd' => Some(D(x))
      case x if x == 's' => Some(S(x))
      case x if x == 'z' => Some(Z(x))
      case x if x == 'n' => Some(N(x))
      case x if x == 'l' => Some(L(x))
      case x if x == 'ĺ' => Some(Ll(x))
      case x if x == 'r' => Some(R(x))
      case x if x == 'ŕ' => Some(RR(x))
      case _ => None
    }.backtrack

  // Palatal consonants (soundI element, but only consonants)
  val palatalp = Parser
    .charWhere(c => soundISet(c))
    .mapFilter {
      case x if x == 'ş' => Some(Sh(x))
      case x if x == 'j' => Some(J(x))
      case x if x == 'ç' => Some(Tsh(x))
      case x if x == 'c' => Some(Dsh(x))
      case _ => None
    }.backtrack

  // Velar consonants
  val velarp = Parser
    .charWhere(c => soundNeutralSet(c))
    .mapFilter {
      case x if x == 'k' => Some(K(x))
      case x if x == 'g' => Some(G(x))
      case x if x == 'x' => Some(X(x))
      case x if x == '\u0295' => Some(Xg(x))
      case _ => None
    }.backtrack

  // Post-velar consonants
  val postvelarp = Parser
    .charWhere(c => soundASet(c))
    .mapFilter {
      case x if x == 'q' => Some(Q(x))
      case x if x == '\u0127' => Some(Hh(x))
      case x if x == '\u0294' => Some(GlottalStop(x))
      case x if x == '\u0295' => Some(Hhvd(x))
      case _ => None
    }.backtrack


  // All consonants
  val allconsonantsp = Parser.oneOf(VlGlottalStopp :: VlVelRoundFricp :: labialp :: coronalp :: palatalp :: velarp :: postvelarp :: semivowelp :: VlAspp :: Nil)

  // Vowels
  val longvowelp = Parser
    .charWhere(c => longVowelSet(c))
    .mapFilter {
      case x if x == 'î' => Some(Ii(x))
      case x if x == 'ê' => Some(Ee(x))
      case x if x == 'a' => Some(Aa(x))
      case x if x == 'o' => Some(Oo(x))
      case x if x == 'û' => Some(Uu(x))
      case _ => None
    }.backtrack

  val shortvowelp = Parser
    .charWhere(c => shortVowelSet(c))
    .mapFilter {
      case x if x == 'i' => Some(I(x))
      case x if x == 'u' => Some(U(x))
      case x if x == 'e' => Some(E(x))
    }.backtrack

  val allvowelsp = Parser.oneOf(longvowelp :: shortvowelp :: Nil)

  // Parsing any phoneme. Used in parsing function in Main.scala
  val phonemep = Parser.oneOf(allconsonantsp :: allvowelsp :: Nil)

  // Making phonemes
    def makeConsonants(in: Char): ConsonantT = in match {
      // consonants
      case x if x == 'p' => P(x)
      case x if x == 'b' => B(x)
      case x if x == 't' => T(x)
      case x if x == 'd' => D(x)
      case x if x == 'k' => K(x)
      case x if x == 'g' => G(x)
      case x if x == 'q' => Q(x)
      case x if x == 'l' => L(x)
      case x if x == 'ĺ' => Ll(x)
      case x if x == 'ç' => Tsh(x)
      case x if x == 'c' => Dsh(x)
      case x if x == 'h' => H(x)
      case x if x == 'f' => F(x)
      case x if x == 'v' => V(x)
      case x if x == 's' => S(x)
      case x if x == 'z' => Z(x)
      case x if x == 'ş' => Sh(x)
      case x if x == 'j' => J(x)
      case x if x == 'ŕ' => RR(x)
      case x if x == 'r' => R(x)
      case x if x == 'ç' => Tsh(x)
      case x if x == 'c' => Dsh(x)
      case x if x == 'x' => X(x)
      case x if x == '\u0295' => Xg(x)
      case x if x == '\u0127' => Hh(x)
      case x if x == 'm' => M(x)
      case x if x == 'n' => N(x)
      case x if x == 'w' => W(x)
      case x if x == 'y' => Y(x)
      case x if x == '\u0294' => GlottalStop(x)
    }
    

  def makeVowels(in: Char): VowelT = in match {
      // vowels
      case x if x == 'î' => Ii(x)
      case x if x == 'ê' => Ee(x)
      case x if x == 'a' => Aa(x)
      case x if x == 'o' => Oo(x)
      case x if x == 'û' => Uu(x)
      case x if x == 'i' => I(x)
      case x if x == 'u' => U(x)
      case x if x == 'e' => E(x)
      }
  

  // Parsing syllables, i.e. CVCV sequences
  val fullCVp = (allconsonantsp ~ allvowelsp).backtrack

  val xwConsp =
    (VlVelRoundFricp ~ allvowelsp).mapFilter {
      case (a,b) => Some((a,b))
      case null => None
    }.backtrack

  val initialWp: Parser[(ConsonantT,VowelT)] =
    (Parser.start.peek.with1 ~ Parser.charWhere(x => x == 'u') ~ allvowelsp).mapFilter {
      case ((a,b),c) => Some((W('w')),c)
      case null => None
    }.backtrack

  val initialYp: Parser[(ConsonantT,VowelT)] =
    (Parser.start.peek.with1 ~ Parser.charWhere(x => x == 'î') ~ allvowelsp).mapFilter {
      case ((a,b),c) => Some((Y('y')),c)
      case null => None
    }.backtrack

  val finalWp: Parser[(ConsonantT,VowelT)] =
    ((allconsonantsp.peek ~ allvowelsp.peek).with1 ~ Parser.charWhere(x => x == 'u') ~ (allconsonantsp orElse Parser.end).peek).mapFilter {
      case (((a,b),c),d) => Some((W('w'),SpreadedV(null)))
      case null => None
    }.backtrack

  val finalYp: Parser[(ConsonantT,VowelT)] =
    ((allconsonantsp.peek ~ allvowelsp.peek).with1 ~ Parser.charWhere(x => x == 'î') ~ (allconsonantsp orElse Parser.end).peek).mapFilter {
      case (((a,b),c),d) => Some((Y('y'),SpreadedV(null)))
      case null => None
    }.backtrack

  val conjunctionp: Parser[(ConsonantT,VowelT)] =
    (Parser.start.with1 ~ Parser.charWhere(x => x == 'u') ~ Parser.end).mapFilter {
      case ((_,b),_) => Some(GlottalStop('\u0294'),Uu('û'))
      case null => None
    }.backtrack

  val consConsp = (Parser.charWhere(consonantsSet(_)) ~ allconsonantsp.peek).mapFilter {
    case (a,_) if continuantSet(a) =>
      Some((makeConsonants(a),SpreadedV(null)))
      //    case (a,_) => Some((makeConsonants(a),I('i')))
    case (a,_) => Some((makeConsonants(a),NotwrittenI(null)))
    case null => None
  }.backtrack

  val consEndp = (Parser.charWhere(consonantsSet(_)) ~ Parser.end.peek)
    .mapFilter {
      case (a,_) => Some(makeConsonants(a),EmptyV(null))
      case null => None
    }.backtrack

  val vowelVowelp: Parser[(ConsonantT,VowelT)] =
    (allvowelsp.peek.with1 ~ allvowelsp).mapFilter {
      case (_,b) => Some((SpreadedCons(null),b))
      case null => None
    }.backtrack


  val cvcvp = Parser.oneOf(xwConsp :: conjunctionp :: initialWp :: initialYp :: finalWp :: finalYp :: vowelVowelp :: consConsp :: fullCVp :: consEndp :: Nil)
  
  val wordp =
    for
      s: NonEmptyList[(ConsonantT, VowelT)] <- cvcvp.rep
    yield s


  // transforming the output
 
  def transformResult(res: Either[cats.parse.Parser.Error,(String, NonEmptyList[(ConsonantT,VowelT)])]): String = {
    res match {
      case Right(a,b) => spellOutFiltered(b)
        //      case Left(l) => "Parse error at: " ++ l.failedAtOffset.toString() ++ "; expected: " ++ l.expected.head.toString()
      case Left(l) => "FAIL!"
    }
  }

  // spell out the whole word
  def spellOutFiltered(in: NonEmptyList[(ConsonantT,VowelT)]): String = {
  val firststep = filterNVsonor(in)
  firststep.map(x => renderPair(x)).mkString
  }

  // rendering consonant vowel pairs
  def renderPair(pair: (ConsonantT,VowelT)): String = renderPhoneme(pair._1) + renderPhoneme(pair._2)

  // Spelling out phonemes
  def renderPhoneme(in: ConsonantT | VowelT): String = in match {
    // consonants
    case P(x) => x.toString
    case B(x) => x.toString
    case T(x) => x.toString
    case D(x) => x.toString
    case K(x) => x.toString
    case G(x) => x.toString
    case Q(x) => x.toString
    case L(x) => x.toString
    case Ll(x) => x.toString
    case Tsh(x) => x.toString
    case Dsh(x) => x.toString
    case H(x) => x.toString
    case F(x) => x.toString
    case V(x) => x.toString
    case S(x) => x.toString
    case Z(x) => x.toString
    case Sh(x) => x.toString
    case J(x) => x.toString
    case RR(x) => x.toString
    case R(x) => x.toString
    case X(x) => x.toString
    case Xg(x) => x.toString
    case Xw(x) => "xw"
    case Hh(x) => x.toString
    case Hhvd(x) => '\u0295'.toString
    case M(x) => x.toString
    case N(x) => x.toString
    case W(x) => x.toString
    case Y(x) => x.toString
    case GlottalStop(x) => x.toString
    case SpreadedCons(x) => ""
    // vowels
    case Ii(x) => x.toString
    case Ee(x) => x.toString
    case Aa(x) => x.toString
    case Oo(x) => x.toString
    case Uu(x) => x.toString
    case I(x) => x.toString
    case U(x) => x.toString
    case E(x) => x.toString
    case NotwrittenI(_) => 'i'.toString
    case _ => ""
  }

  // Determining which empty V slots will be spelled out with 'i' or not
  def filterNVsonor(in: NonEmptyList[(ConsonantT,VowelT)]): List[(ConsonantT,VowelT)] = {
    val listOfPairs = in.head :: in.tail

    def wordType(wt: List[(ConsonantT,VowelT)]): List[(ConsonantT,VowelT)] = wt match {
      case (S(_),EmptyV(_)) :: Nil => (S('s'),I('i')) :: Nil
      case (Sh(_),EmptyV(_)) :: Nil => (S('ş'),I('i')) :: Nil
      case (S(_),NotwrittenI(_)) :: (a: StopT, b) :: xs if !wt.tail.isEmpty =>
        (S('s'),SpreadedV(null)) :: (a,b) :: filterHelper(xs)
      case (Sh(_),NotwrittenI(_)) :: (a: StopT, b) :: xs if !wt.tail.isEmpty =>
        (Sh('ş'),SpreadedV(null)) :: (a,b) :: filterHelper(xs)
      case (S(_),SpreadedV(_)) :: (a: StopT, b) :: xs if !wt.tail.isEmpty =>
        (S('s'),SpreadedV(null)) :: (a,b) :: filterHelper(xs)
      case (Sh(_),SpreadedV(_)) :: (a: StopT, b) :: xs if !wt.tail.isEmpty =>
        (Sh('ş'),SpreadedV(null)) :: (a,b) :: filterHelper(xs)
      case (a,EmptyV(_)) :: Nil => (a,I('i')) :: Nil
      case (a,NotwrittenI(_)) :: xs if !wt.tail.isEmpty => (a,I('i')) :: filterHelper(xs)
      case (a,SpreadedV(_)) :: xs if !wt.tail.isEmpty => (a,I('i')) :: filterHelper(xs)
      case _ => filterHelper(wt)
    }


    def filterHelper(a: List[(ConsonantT,VowelT)]): List[(ConsonantT,VowelT)] =
      a match {
        // vowel-vowel word medially
        // Semivowel Ii-Y
        case (SpreadedCons(_),Ii(_)) :: (SpreadedCons(_),d) :: Nil =>
          (SpreadedCons(null),d) :: Nil
        case (a,Ii(_)) :: (SpreadedCons(_),d) :: (SpreadedCons(_),f) :: Nil =>
          (a,Ii('î')) :: (Y('y'),f) :: Nil
        case (a,Ii(_)) :: (SpreadedCons(_),d) :: Nil =>
          (a,Ii('î')) :: (Y('y'),d) :: Nil
        case (a,Ii(_)) :: (SpreadedCons(_),d) :: xs =>
          (a, Ii('î')) :: (Y('y'),d) :: filterHelper(xs)
        case z :: (a,Ii(_)) :: (SpreadedCons(_),d) :: xs =>
          z :: (a,Ii('î')) :: (Y('y'),d) :: filterHelper(xs)
        // Semivowel U-W
        case (a,U(_)) :: (SpreadedCons(_),d) :: Nil =>
          (a,EmptyV(null)) :: (W('w'),d) :: Nil
        case (a,U(_)) :: (SpreadedCons(_),d) :: xs =>
          (a,EmptyV(null)) :: (W('w'),d) :: filterHelper(xs)
        case z :: (a,U(_)) :: (SpreadedCons(_),d) :: xs =>
          z :: (a,EmptyV(null)) :: (W('w'),d) :: filterHelper(xs)
        // other vowels in the first position
        case (a,b: SoundAT) :: (SpreadedCons(_),d: (SoundIT | SoundAT)) :: Nil =>
          (a,b) :: (Y('y'),d) :: Nil
        case (a,b: SoundAT) :: (SpreadedCons(_),d: (SoundIT | SoundAT)) :: xs =>
          (a,b) :: (Y('y'),d) :: filterHelper(xs)
        case z :: (a,b: SoundAT) :: (SpreadedCons(_),d: (SoundIT | SoundAT)) :: xs =>
          z :: (a,b) :: (Y('y'),d) :: filterHelper(xs)
          // 2nd vowel is u
        case (a,b) :: (SpreadedCons(_),U(_)) :: Nil =>
          (a,b) :: (W('w'),EmptyV(null)) :: Nil
        case (a,b) :: (SpreadedCons(_),U(_)) :: xs =>
          (a,b) :: (W('w'),EmptyV(null)) :: filterHelper(xs)
        case z :: (a,b) :: (SpreadedCons(_),U(_)) :: xs =>
          z :: (a,b) :: (W('w'),EmptyV(null)) :: filterHelper(xs)
        // consonant sequences in input
        case (a,SpreadedV(_)) :: (c,SpreadedV(_)) :: Nil =>
          (a,I('i')) :: (c,EmptyV(null)) :: Nil
        case (a,SpreadedV(_)) :: (c,SpreadedV(_)) :: xs =>
          (a,I('i')) :: (c,SpreadedV(null)) :: filterHelper(xs)
        case z :: (a,SpreadedV(_)) :: (c,SpreadedV(_)) :: xs =>
          z :: (a,I('i')) :: (c,SpreadedV(null)) :: filterHelper(xs)
        case (a: NasalT,SpreadedV(null)) :: Nil => (a,SpreadedV(null)) :: Nil
          // when to spell out NotwrittenI
        case (a,NotwrittenI(_)) :: (c: StopT,EmptyV(_)) :: Nil if a.sonority > c.sonority =>
          (a,SpreadedV(null)) :: (c,EmptyV(null)) :: Nil
        case (a,NotwrittenI(_)) :: (c,EmptyV(_)) :: Nil =>
          (a,I('i')) :: (c,EmptyV(null)) :: Nil
        case (a,SpreadedV(_)) :: (c: NasalT,EmptyV(_)) :: Nil =>
          (a,I('i')) :: (c,EmptyV(null)) :: Nil
        case (a,NotwrittenI(_)) :: (c: NoiseT,SpreadedV(_)) :: xs =>
          (a,I('i')) :: (c,EmptyV(null)) :: filterHelper(xs)
        case (a: (NoiseT | NasalT | LateralT),NotwrittenI(_)) :: (c: ConsonantT,d: NotNeutralT) :: Nil =>
          (a,EmptyV(null)) :: (c,d) :: Nil
          // Another attempt
        case (a, SpreadedV(_)) :: (c: StopT, NotwrittenI(_)) :: (e: (NoiseT | NasalT | LateralT),f) :: Nil =>
          (a, NotwrittenI(null)) :: (c, I('i')) :: (e,f) :: Nil
        case (a, SpreadedV(_)) :: (c: StopT, NotwrittenI(_)) :: (e: (NoiseT | NasalT | LateralT),f) :: xs =>
          (a, NotwrittenI(null)) :: (c, I('i')) :: (e,f) :: filterHelper(xs)
        // previous
        // case (a: (NoiseT | NasalT | LateralT),NotwrittenI(_)) :: (c: ConsonantT,d: NotNeutralT) :: xs =>
        //   (a,EmptyV(null)) :: (c,d) :: filterHelper(xs)
        case z :: (a: (NoiseT | NasalT | LateralT),NotwrittenI(_)) :: (c: ConsonantT,d: NotNeutralT) :: xs =>
          z :: (a,EmptyV(null)) :: (c,d) :: filterHelper(xs)
        case (a,NotwrittenI(_)) :: xs :: Nil if a.sonority >= xs._1.sonority && !xs._1.withi =>
               (a,EmptyV(null)) :: xs :: Nil
//        case x :: xs :: Nil  => x :: xs :: Nil
        case (a,NotwrittenI(_)) :: xs if !xs.isEmpty && a.sonority >= xs.head._1.sonority && !xs.head._1.withi =>
          (a,EmptyV(null)) :: xs.head :: filterHelper(xs.tail)
        case x :: xs if !xs.isEmpty => x :: filterHelper(xs)
        case x :: xs => x :: Nil      
        case _ => a.toList
      }
    filterHelper(wordType(listOfPairs))
  }

}
